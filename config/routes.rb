Rails.application.routes.draw do
  resources :reservations
  get 'home/index'

  root 'home#index' #Deixa como padrão a página inicial
  

  resources :books
  resources :authors
  resources :clients
  resources :librarians
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
